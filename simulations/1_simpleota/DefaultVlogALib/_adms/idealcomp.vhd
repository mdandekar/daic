LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

LIBRARY STD;
USE STD.ALL;

ENTITY idealcomp IS
  GENERIC (
   vlogic_hi :   integer  := 1;
   vlogic_lo :   integer  := 0;
   trise :   real :=  1.000000e-11;
   tfall :   real :=  1.000000e-11;
   RC_const :   real :=  1.000000e-11 );
  PORT (
    SIGNAL INP : IN STD_LOGIC;
    SIGNAL INM : IN STD_LOGIC;
    SIGNAL \OUT\ : OUT STD_LOGIC);
END ENTITY idealcomp;

