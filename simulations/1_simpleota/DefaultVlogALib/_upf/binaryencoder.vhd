library ieee;
use ieee.std_logic_1164.all;

use ieee.upf.all;

entity binaryencoder is
  generic (
   vdd :   integer  := 1 );
  PORT (
    signal comp1 : IN std_logic;
    signal comp2 : IN std_logic;
    signal Clk : IN std_logic;
    signal P2 : IN std_logic;
    signal b0 : OUT std_logic;
    signal b1 : OUT std_logic;
    signal b2 : OUT std_logic);
end entity binaryencoder;

