library ieee;
use ieee.std_logic_1164.all;

use ieee.upf.all;

entity clockedintegrator is
  generic (
   sigctrl_trans :   integer  := 0;
   clock_frequency :   integer  := 1 );
  PORT (
    signal \in\ : INOUT std_logic;
    signal \out\ : INOUT std_logic;
    signal clk : INOUT std_logic;
    signal gnd : INOUT std_logic);
end entity clockedintegrator;

