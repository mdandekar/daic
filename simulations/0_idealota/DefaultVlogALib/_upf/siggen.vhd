library ieee;
use ieee.std_logic_1164.all;

use ieee.upf.all;

entity siggen is
  generic (
   f :   real :=  1.000000e+06;
   DC :   integer  := 0 );
  PORT (
    signal outp : INOUT std_logic;
    signal outn : INOUT std_logic;
    signal nss : INOUT std_logic);
end entity siggen;

