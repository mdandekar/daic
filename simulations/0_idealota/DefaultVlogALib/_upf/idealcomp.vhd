library ieee;
use ieee.std_logic_1164.all;

use ieee.upf.all;

entity idealcomp is
  generic (
   vlogic_hi :   integer  := 1;
   vlogic_lo :   integer  := 0;
   trise :   real :=  1.000000e-11;
   tfall :   real :=  1.000000e-11;
   RC_const :   real :=  1.000000e-11 );
  PORT (
    signal INP : IN std_logic;
    signal INM : IN std_logic;
    signal \OUT\ : OUT std_logic);
end entity idealcomp;

