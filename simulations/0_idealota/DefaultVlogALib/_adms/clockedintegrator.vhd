LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

LIBRARY STD;
USE STD.ALL;

ENTITY clockedintegrator IS
  GENERIC (
   sigctrl_trans :   integer  := 0;
   clock_frequency :   integer  := 1 );
  PORT (
    SIGNAL \in\ : INOUT STD_LOGIC;
    SIGNAL \out\ : INOUT STD_LOGIC;
    SIGNAL clk : INOUT STD_LOGIC;
    SIGNAL gnd : INOUT STD_LOGIC);
END ENTITY clockedintegrator;

